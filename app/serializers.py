from .models import *
from rest_framework import serializers
class GeneralProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['first_name', 'last_name']
class GeneralProfileSerializerForSupport(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', "address_count"]
class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = "__all__"