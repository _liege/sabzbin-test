from django.db import models
from django.contrib.auth.models import User as U


class User(U):
    phone = models.CharField(max_length=20)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)
    @property
    def address_count(self):
        return self.address.all().count()
class SupportManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(creator = "SUPPORT")


class Address(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="address")
    title = models.CharField(max_length=100)
    CHOICES = (
        ("USER", "user"),
        ("SUPPORT", "support")
    )
    creator = models.CharField(max_length=20, choices=CHOICES)
    location = models.CharField(max_length=200)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)
    objects = models.Manager()
    support = SupportManager()


