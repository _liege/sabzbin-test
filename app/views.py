from django.shortcuts import render
from rest_framework.views import APIView
from django.http import HttpResponse
from .serializers import *
from .models import *
from django.db.models import Value, IntegerField, Count, F
from rest_framework.response import Response
# Create your views here.
def filter_queryset(queryset):
    return queryset.filter(creator = "SUPPORT")
def add_address_count(queryset):
    return queryset.annotate(add_count = Count(F('address')) )
def send_notif(serializer):
    print(serializer.data)
class TestingQueryMethods1(APIView):
    def get(self, request):
        # adding column add_count(address_count) to queryset
        user = User.objects.filter(id = 2)
        counted_address = add_address_count(user)
        #just address created by supports
        address = Address.objects.all()
        filtered_queryset = filter_queryset(address)
        print(filtered_queryset)
        print(counted_address[0].add_count)
        return HttpResponse("hello")
class Users(APIView):
    def get(self, request):
        method = request.GET.get("method")
        address_num = request.GET.get("address")
        if method == "gte":
            users = User.objects.all()
            users = add_address_count(users)
            users = users.filter(add_count__gte = int(address_num))
            serializer = GeneralProfileSerializerForSupport(users, many = True)
            return Response(serializer.data)
        elif method == "lt":
            users = User.objects.all()
            users = add_address_count(users)
            users = users.filter(add_count__lt = int(address_num))
            serializer = GeneralProfileSerializerForSupport(users, many = True)
            return Response(serializer.data)
        else:
            users = User.objects.all()
            users = add_address_count(users)
            serializer = GeneralProfileSerializerForSupport(users, many = True)
            return Response(serializer.data)
    def post(self,request):
        data = request.data.copy()
        data.update({"creator": "SUPPORT"})
        serializer = AddressSerializer(data= data)
        serializer.is_valid()
        serializer.save()
        send_notif(serializer)
        return Response(serializer.data)
# class login(APIView):
